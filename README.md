This project has been moved to https://github.com/gbouthenot/dmwikimap

Bitbucket deleted all my mercurial repositories a few year ago, I had to convert it to git and host it elsewhere (I won't put my work on bitbucket ever again !).